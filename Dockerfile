FROM openjdk:8-jdk

COPY hello-world.yml /conf/hello-world.yml
COPY /target/dropwizard-helloworld-1.0-SNAPSHOT.jar /app/dropwizard-helloworld-1.0-SNAPSHOT.jar

WORKDIR /app/

RUN java -version

#CMD ["java","-jar","dropwizard-helloworld-1.0-SNAPSHOT.jar","server","/root/dropwizard-helloworld/dropwizard-helloworld/hello-world.yml"]
CMD java -jar /app/dropwizard-helloworld-1.0-SNAPSHOT.jar server /conf/hello-world.yml
EXPOSE 8080-8081
